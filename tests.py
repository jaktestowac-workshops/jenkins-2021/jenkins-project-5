import unittest


class ShopMockedTests(unittest.TestCase):
    def setUp(self):
        print('Setting up tests...')
        self.mocked_actual_title = 'Demobank - Bankowość Internetowa - Logowanie'

    def tearDown(self):
        print('Tearing Down up tests...')

    def test_page_title(self):
        expected_title = 'Demobank - Bankowość Internetowa - Logowanie'
        actual_title = self.mocked_actual_title
        self.assertEqual(expected_title, actual_title)


if __name__ == '__main__':
    unittest.main()